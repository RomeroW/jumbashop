<?php

$routes->get('/', 'app\controllers\Products::listAll');
$routes->get('/product/{id}', 'app\controllers\Products::one');

$routes->get('/cart', 'app\controllers\Products::showCart');

$routes->get('/cart/checkout/{userId}', 'app\controllers\Products::checkout');
$routes->post('/cart/checkout/{userId}', 'app\controllers\Products::checkout');

$routes->post('/product/save', 'app\controllers\Products::save');

$routes->get('/product/add-to-cart/{userId}/{id}', 'app\controllers\Products::addToCart');
$routes->get('/product/del-from-cart/{userId}/{id}', 'app\controllers\Products::delFromCart');

