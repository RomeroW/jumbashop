<?php
/**
 * Created by PhpStorm.
 * User: romero
 * Date: 11/1/14
 * Time: 1:08 PM
 */

namespace app\models;


use mako\redis\Redis;
use mako\session\Session;

/**
 * Class ActiveUser
 * @package app\models
 * @property $redis Redis
 */
class ActiveUser
{

    private $usersTable = 'active_users';

    public $session;
    public $redis;

    public $id;

    private $attributes;

    /**
     * @var Cart
     */
    public $cart;

    /**
     * Key - Field Name,
     * Value - Regex
     *
     * @var array
     */
    private $validationRules = [
        'email' => '/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/',
        'phone' => '/^\+[1-9]{1}[0-9]{7,16}$/',
        'name'  => ''
    ];

    public function __construct(Redis $redis, Session $session)
    {
        $this->redis = $redis;

        $this->session = $session;
        $this->id = $this->session->getId();

        //Update redis active users table
        $this->redis->hset($this->usersTable, $this->id, time());

        $this->cart = new Cart($this);
    }

    /**
     * Transfers table - list in redis
     * @return string
     */
    public function getTransfersTable()
    {
        return $this->id . '_transfers';
    }

    /**
     * Cart table - hash set in redis
     * @return string
     */
    public function getCartTable()
    {
        return $this->id . '_cart_' . $this->getLastCartIndex();
    }

    /**
     * Last Cart Index key
     * @return string
     */
    public function getLCIKey()
    {
        return $this->id . '_last_cart_index';
    }

    /**
     * Last cart index
     * @return mixed
     */
    public function getLastCartIndex()
    {
        if(!$this->redis->exists($this->getLCIKey())) {
            return 0;
        }

        return $this->redis->get(
            $this->getLCIKey()
        );
    }

    public function incrementCartIndex()
    {
        $incremented = $this->getLastCartIndex() + 1;
        return $this->redis->set(
            $this->getLCIKey(),
            $incremented
        );
    }

    /**
     * Add visited product id to storage
     * @param $id
     * @return mixed
     */
    public function setVisitedProduct($id)
    {
        return $this->redis->rpush(
            $this->getTransfersTable(),
            $id
        );
    }

    public function validate($data)
    {
        $errors = array();

        foreach($this->validationRules as $field => $rule) {

            if(strlen(trim($data[$field])) == 0 && $rule == '') {
                $errors[] = ucfirst($field) . ' is required.';
                continue;
            }
            elseif(strlen(trim($data[$field])) != 0 && $rule == '') {
                continue;
            }

            if(!preg_match($rule, $data[$field])) {
                unset($data[$field]);
                $errors[] = ucfirst($field) . ' is not OK.';
            }
        }

        $this->attributes = $data;

        return $errors;
    }

    public function value($key)
    {
        if(isset($this->attributes[$key])) {
            return $this->attributes[$key];
        }

        return '';
    }

} 