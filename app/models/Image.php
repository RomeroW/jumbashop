<?php

namespace app\models;

class Image extends \mako\database\midgard\ORM
{
    protected $tableName = 'js_images';

    public function product()
    {
        return $this->hasOne('app\models\Product', 'product');
    }
}