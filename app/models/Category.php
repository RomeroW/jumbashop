<?php

namespace app\models;

class Category extends \mako\database\midgard\ORM
{
    protected $tableName = 'js_categories';

    public function products()
    {
        return $this->hasMany('app\models\Product', 'cat_id');
    }
}