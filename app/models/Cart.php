<?php
/**
 * Created by PhpStorm.
 * User: romero
 * Date: 11/1/14
 * Time: 4:59 PM
 */

namespace app\models;


use mako\session\Session;
use mako\session\stores\Redis;
use mako\utility\Arr;

/**
 * Class Cart
 * @package app\models
 */
class Cart {

    /**
     * @var $user ActiveUser
     */
    private $user;

    private $cart = array();

    public function __construct(ActiveUser $user)
    {
        $this->user = $user;

        $sessionCart = $user->session->get('cart', false);
        if($sessionCart) {
            $this->cart = unserialize($sessionCart);
        }
    }

    /**
     * Add product to cart
     * @param $id int Product id
     */
    public function add($id)
    {
        $this->user->redis->hset(
            $this->user->getCartTable(),
            $id,
            0
        );

        Arr::set($this->cart, $id, '0');

        $this->user->session->put('cart', serialize($this->cart));
    }

    /**
     * Remove product from cart
     * @param $id int Product id
     */
    public function remove($id)
    {
        Arr::delete($this->cart, $id);

        $this->user->session->put('cart', serialize($this->cart));
    }

    /**
     * Store bought products to redis
     */
    public function buy()
    {
        foreach($this->cart as $productId => $isBought) {
            $product = Product::get($productId);

            if($product) {
                $product->bought++;
                $product->save();
            }

            $this->user->redis->hset(
                $this->user->getCartTable(),
                $productId,
                1
            );
        }
        $this->user->incrementCartIndex();

        $this->user->session->put('cart', serialize(array()));

    }

    public function getProducts()
    {
        return array_keys($this->cart);
    }

} 