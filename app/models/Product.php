<?php

namespace app\models;

use app\helpers\User;

class Product extends \mako\database\midgard\ORM
{
    protected $tableName = 'js_products';

    public function category()
    {
        return $this->hasOne('app\models\Category', 'cat_id');
    }

    public function images()
    {
        return $this->hasMany('app\models\Image', 'product');
    }

    public function getBuyLink()
    {
        return Product::getBuyLinkS($this->id);
    }

    public static function getBuyLinkS($id) {
        return '/product/add-to-cart/' . User::get()->id . '/' . $id;
    }

    public function getRemLink()
    {
        return '/product/del-from-cart/' . User::get()->id . '/' . $this->id;
    }

    public function getImageLink()
    {
        return $this->hasMany('app\models\Image', 'product')->first()->link;
    }

    //SELECT id, bought, views, in_cart FROM `js_products` ORDER BY bought DESC, views DESC, in_cart DESC
}