<?php

namespace app\controllers;

use app\models\Product;
use mako\application\services\SessionService;
use mako\config\Config;
use mako\database\ConnectionManager;
use \mako\http\Request;
use mako\http\RequestException;
use \mako\http\Response;
use \mako\redis\Redis;
use app\models\ActiveUser;
use app\helpers\User;
use mako\http\routing\PageNotFoundException;
use mako\http\routing\URLBuilder;
use mako\reactor\tasks\App;
use mako\session\Session;
use mako\tests\unit\syringe\Store;
use \mako\view\ViewFactory;

class Main extends \mako\http\routing\Controller
{
    /**
     * View factory instance.
     *
     * @var \mako\view\ViewFactory
     */

    protected $viewFactory;

    protected $config;
    protected $urlBuilder;
    protected $connection;

    /**
     * Constructor.
     *
     * @access  public
     * @param   \mako\http\Request      $request      Request instance
     * @param   \mako\http\Response     $response     Response instance
     * @param   \mako\view\ViewFactory  $viewFactory  View factory instance
     */

    public function __construct(Request $request,
                                Response $response,
                                Config $config,
                                ViewFactory $viewFactory,
                                Session $session,
                                URLBuilder $urlBuilder,
                                ConnectionManager $connection)
    {
        parent::__construct($request, $response);

        $this->config = $config;
        $this->urlBuilder = $urlBuilder;
        $this->connection = $connection->connection();

        $this->viewFactory = $viewFactory;

        User::set(new ActiveUser(new Redis($config->get('redis')['configurations']['localhost']), $session));
    }
}
