<?php

namespace app\controllers;

use app\models\Image;
use app\models\Product;
use app\helpers\User;
use mako\utility\Arr;

class Products extends Main
{

    private function getFourRand($data)
    {
        return $this->array_random_assoc($data, 4);
    }

    private function array_random_assoc($arr, $num = 1)
    {
        $keys = array_keys($arr);
        shuffle($keys);

        $r = array();
        for ($i = 0; $i < $num; $i++) {
            if(!array_key_exists($i, $keys)) continue;
            $r[$keys[$i]] = $arr[$keys[$i]];
        }
        return $r;
    }

    public function listAll()
    {
        $products = Product::select(['*'])->
            orderBy('bought', 'desc')->
            orderBy('views', 'desc')->
            orderBy('in_cart', 'desc')->
            all();

        return $this->viewFactory->create('product.list', [
            'products' => $products
        ]);
    }

    public function one($id)
    {
        $product = Product::get($id);

        if(!$product)
            throw new PageNotFoundException("Page not found");

        $relatedProducts = $this->getFourRand($this->connection->all(
                'select p.*, i.link from js_products_relationships pr left join js_products p on pr.related_id=p.id left join js_images as i on p.id=i.product where pr.product_id=? and pr.rel_type=1 group by p.id  order by pr.weight desc limit 10',
                [$id]));

        $inCartProducts = $this->getFourRand($this->connection->all(
                'select p.*, i.link from js_products_relationships pr left join js_products p on pr.related_id=p.id left join js_images as i on p.id=i.product where pr.product_id=? and pr.rel_type=2 group by p.id  order by pr.weight desc limit 10',
                [$id]));

        $boughtProducts = $this->getFourRand($this->connection->all(
                'select p.*, i.link from js_products_relationships pr left join js_products p on pr.related_id=p.id left join js_images as i on p.id=i.product where pr.product_id=? and pr.rel_type=3 group by p.id order by pr.weight desc limit 10',
                [$id]));


        $product->views++;
        $product->save();

        User::get()->setVisitedProduct($id);

        $images = Image::where('product', '=', $id)->all();

        return $this->viewFactory->create('product.one', [
            'product' => $product,
            'images' => $images,
            'relatedProducts' => $relatedProducts,
            'boughtProducts' => $boughtProducts,
            'inCartProducts' => $inCartProducts
        ]);
    }

    public function addToCart($userId, $id)
    {
        if($userId !== User::get()->id) {
            return $this->response->redirect('/product/' . $id);
        }

        $product = Product::get($id);
        if(!$product)
            throw new PageNotFoundException("Page not found");
        $product->in_cart++;
        $product->save();

        User::get()->cart->add($id);
        return $this->response->redirect('/product/' . $id);

    }


    public function delFromCart($userId, $id)
    {
        if($userId !== User::get()->id) {
            return $this->response->redirect('/cart');
        }

        User::get()->cart->remove($id);
        return $this->response->redirect('/cart');
    }

    public function showCart()
    {
        $cartData = User::get()->cart->getProducts();

        $products = count($cartData) != 0 ? Product::in(
            'id',
            $cartData
        )->all() : array();

        if(count($products) == 0) {
            return $this->response->redirect('/');
        }

        $total = 0;
        foreach($products as $product) {
            $total += $product->price;
        }

        return $this->viewFactory->create('cart.list', [
            'products' => $products,
            'total' => $total
        ]);
    }

    public function checkout($userId)
    {
        if($userId !== User::get()->id) {
            return $this->response->redirect('/cart');
        }

        if(isset($_POST['User'])) {
            $errors = User::get()->validate($_POST['User']);

            if(count($errors) > 0) {
                return $this->viewFactory->create('cart.checkout', [
                    'errors' => $errors
                ]);
            }

            User::get()->cart->buy();
            return $this->viewFactory->create('cart.done');
        }

        return $this->viewFactory->create('cart.checkout');
    }

}
