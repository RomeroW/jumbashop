<?php
namespace app\tasks;


use mako\application\services\DatabaseService;
use mako\auth\user\User;
use mako\cache\stores\Database;
use mako\config\Config;
use mako\reactor\io\Input;
use \mako\redis\Redis;
use mako\utility\Arr;
use mako\database\Connection;
use mako\database\ConnectionManager;
use Symfony\Component\Console\Output\Output;

class Data extends \mako\reactor\Task
{
    protected static $taskInfo = array
    (
        'saveAll' => array
        (
            'description' => 'Store data about products to database.',
        ),
    );

    private $redis;
    private $connection;

    /**
     * @var int Time gap that session is live in Redis (in seconds)
     */
    private $sessionActiveTime = 1200;

    public function __construct(Input $input, Output $output, Config $config, ConnectionManager $database)
    {
        parent::__construct($input, $output);

        $this->redis = new Redis($config->get('redis.configurations.localhost'));
        $this->connection = $database->connection();//new Connection('database', $config->get('database.configurations.lh'));
    }

    public function saveAll()
    {
        $activeUsers = $this->redis->hgetall('active_users');

        for($i = 0; $i < count($activeUsers); $i = $i+2) {
            if ((time() - $activeUsers[$i+1]) > $this->sessionActiveTime) {

                $this->output->write('Processing <yellow>' . $activeUsers[$i] . '</yellow>...');

                $this->processViews($activeUsers[$i]);
                $this->processCart($activeUsers[$i]);

                $this->output->writeln('<green>Done!</green>');

                $this->redis->hdel('active_users', $activeUsers[$i]);
            }
        }
    }

    private function processViews($userId)
    {
        $this->output->nl();
        //$this->output->error(' [FAILED]');
        $this->output->write("\tValidating views... ");
        $views = $this->redis->lrange($userId . '_transfers', 0, -1);

        $validViewsPairs = array();
        for($i = 0; $i < count($views) - 1; $i++) {
            if($views[$i] == $views[$i + 1] || Arr::has($validViewsPairs, $views[$i] . ' ' . $views[$i + 1])) {
                continue;
            }
            Arr::set($validViewsPairs, $views[$i] . ' ' . $views[$i + 1], '');
        }

        $validViews = array();
        $i = 1;
        foreach($validViewsPairs as $keys => $empty) {
            $path = explode(' ', $keys);

            $validViews[] = $path[0];
            if($i == count($validViewsPairs)) {
                $validViews[] = $path[1];
            }

            $i++;
        }
        $this->output->writeln("<green>Done!</green>");

        $this->output->write("\tSaving views relationship to persistent storage... ");
        $this->storeViewData($validViews, $this->getP2PKey(), "\t\t\t");
        $this->output->write("\tRemoving temporary data from Redis... ");
        $this->redis->del($userId . '_transfers');
        $this->output->writeln("<green>Done!</green>");

        $this->output->writeln("\t<green>Done!</green>");
    }

    private function processCart($userId)
    {
        $this->output->nl();
        //$this->output->error(' [FAILED]');
        $this->output->writeln("\tProcessing cart data... ");

        $lastCartIndex = $this->redis->get($userId . '_last_cart_index');
        for($i = 0; $i < $lastCartIndex + 1; $i++) {
            $cartInfo = $this->redis->hgetall($userId . '_cart_' . $i);

            $addedToCart = array();
            $bought = array();

            for($j = 0; $j < count($cartInfo) - 1; $j++) {
                $addedToCart[] = $cartInfo[$j];

                if($cartInfo[$j+1] == 1) {
                    $bought[] = $cartInfo[$j];
                }
            }

            $this->output->write("\t\tStoring cart({$i}) data... ");
            $this->storeCartData($addedToCart, $this->getPCKey(), "\t\t");

            $this->output->write("\t\tStoring bought data from cart({$i})... ");
            $this->storeCartData($bought, $this->getPBKey(), "\t\t");

            $this->redis->del($userId . '_cart_' . $i);
        }
        $this->redis->del($userId . '_last_cart_index');

        $this->output->writeln("\t<green>Done!</green>");
    }

    private function storeViewData($items, $relKey, $tabs)
    {
        $insertItems = array();

        //build select sql
        $selectSql = 'select * from js_products_relationships where (product_id, related_id, rel_type) in (';

        $selectItems = array();
        for ($i = 0; $i < count($items) - 1; $i++) {
            for ($j = $i+1; $j < count($items); $j++) {
                $fromId = $items[$i];
                $toId = $items[$j];

                if ($fromId == $toId) continue;

                $selectItems[] = "({$fromId}, {$toId}, {$relKey})";

                $insertItems["{$fromId} {$toId}"] = '';
            }
        }
        if(count($selectItems) > 0) {
            $selectSql .= implode(',', $selectItems) . ')';
            $items = $this->connection->all($selectSql);
        }
        else {
            $this->output->writeln('<green>Done!</green>');
            return true;
        }

        $update = false;
        $insert = false;

        $updateSql = 'update js_products_relationships set weight=weight+1 where (product_id, related_id, rel_type) in (';
        $updateItems = array();
        foreach($items as $item) {
            Arr::delete($insertItems, "{$item->product_id} {$item->related_id}");

            $updateItems[] = "({$item->product_id}, {$item->related_id}, {$relKey})";
        }
        if(count($updateItems)) {
            $updateSql .= implode(',', $updateItems) . ')';
            $update = $this->connection->query($updateSql);
        }

        $insertSql = 'insert into js_products_relationships (product_id, related_id, rel_type) values ';
        $insertItemsSQL = array();
        foreach($insertItems as $keys => $empty) {
            $keysData = explode(' ', $keys);
            $insertItemsSQL[] = "({$keysData[0]}, {$keysData[1]}, {$relKey})";
        }
        if(count($insertItemsSQL) > 0) {
            $insertSql .= implode(',', $insertItemsSQL);
            $insert = $this->connection->query($insertSql);
        }

        if($update || $insert) {
            $this->output->writeln('<green>Done!</green>');
        }
        else {
            $this->output->error('fail');
        }
    }
    private function storeCartData($items, $relKey, $tabs)
    {
        $insertItems = array();

        //build select sql
        $selectSql = 'select * from js_products_relationships where (product_id, related_id, rel_type) in (';

        $selectItems = array();
        for ($i = 0; $i < count($items); $i++) {
            for ($j = 0; $j < count($items); $j++) {
                $fromId = $items[$i];
                $toId = $items[$j];

                if ($fromId == $toId) continue;

                $selectItems[] = "({$fromId}, {$toId}, {$relKey})";

                $insertItems["{$fromId} {$toId}"] = '';
            }
        }
        if(count($selectItems) > 0) {
            $selectSql .= implode(',', $selectItems) . ')';
            $items = $this->connection->all($selectSql);
        }
        else {
            $this->output->writeln("<green>Done!</green>");
            return true;
        }

        $update = false;
        $insert = false;

        $updateSql = 'update js_products_relationships set weight=weight+1 where (product_id, related_id, rel_type) in (';
        $updateItems = array();
        foreach($items as $item) {
            Arr::delete($insertItems, "{$item->product_id} {$item->related_id}");

            $updateItems[] = "({$item->product_id}, {$item->related_id}, {$relKey})";
        }
        if(count($updateItems)) {
            $updateSql .= implode(',', $updateItems) . ')';
            $update = $this->connection->query($updateSql);
        }

        $insertSql = 'insert into js_products_relationships (product_id, related_id, rel_type) values ';
        $insertItemsSQL = array();
        foreach($insertItems as $keys => $empty) {
            $keysData = explode(' ', $keys);
            $insertItemsSQL[] = "({$keysData[0]}, {$keysData[1]}, {$relKey})";
        }
        if(count($insertItemsSQL) > 0) {
            $insertSql .= implode(',', $insertItemsSQL);
            $insert = $this->connection->query($insertSql);
        }

        if($update || $insert) {
            $this->output->writeln("<green>Done!</green>");
        }
        else {
            $this->output->error('fail');
        }
    }

    private function getP2PKey()
    {
        return 1;
    }

    private function getPCKey()
    {
        return 2;
    }

    private function getPBKey()
    {
        return 3;
    }
} 