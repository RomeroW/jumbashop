<?php
/**
 * Created by PhpStorm.
 * User: romero
 * Date: 11/1/14
 * Time: 6:14 PM
 */

namespace app\helpers;

class User {
    /**
     * @var app\models\ActiveUser
     */
    private static $user;

    public static function set($user)
    {
        self::$user = $user;
    }

    public static function get()
    {
        return self::$user;
    }

} 