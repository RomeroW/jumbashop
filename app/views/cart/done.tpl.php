{% extends:'layout' %}
{% block:content %}
<div class="small-11 medium-7 large-6 small-centered columns" style="text-align: center">
    <h2>Products bought!</h2>
    <p>Our manager will call you in several minutes.</p>
    <a class="back" href="/">Go back to the products list</a>
</div>
{% endblock %}