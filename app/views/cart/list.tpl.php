{% extends:'layout' %}
{% block:content %}

<div id="cart-buttons" class="row">
    <div class="small-6 columns">
        <a class="button checkout" href="/cart/checkout/{{app\helpers\User::get()->id}}">
            <div class="small-checkout-icon"></div>
            <div class="button-label">Checkout cart</div>
        </a>
    </div>
    <div class="total-in-cart small-6 columns" style="text-align: right">
        Total: {{$total}}$
    </div>
</div>
<div style="clear: both"></div>
<div id="products" class="row">
    <h2 class="also">In your cart</h2>
    <ul class="large-block-grid-4 medium-block-grid-3 small-block-grid-1">
        {% foreach($products as $product) %}
        <li>
            <div class="product-item">
                <div class="image">
                    <img src="{{$product->getImageLink()}}">
                </div>
                <div class="info">
                    <a class="title" href="/product/{{$product->id}}">{{$product->title}}</a>
                    <div class="bottom rows">
                        <a class="button item-from-cart" href="{{$product->getRemLink()}}">Remove</a>
                        <div class="price">{{$product->price}}$</div>
                        <div style="clear: both"></div>
                    </div>
                </div>
            </div>
        </li>
        {% endforeach %}
    </ul>
</div>
{% endblock %}