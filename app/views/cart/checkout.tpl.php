{% extends:'layout' %}
{% block:content %}

<div class="row">
    <div class="small-11 medium-7 large-6 small-centered columns">
        <h2 class="also">
            Just left us your contact data, and our manager will call you in several minutes
        </h2>
        {% if(isset($errors)) %}
        <ul class="errors">
            {% foreach($errors as $error) %}
            <li>{{$error}}</li>
            {% endforeach %}
        </ul>
        {% endif %}
        <form action="/cart/checkout/{{app\helpers\User::get()->id}}" method="post">
            <label for="userName">
                Name:
                <input id="userName" name="User[name]" type="text" value="{{app\helpers\User::get()->value('name')}}"/>
            </label><br/>
            <label for="userPhone">
                Phone number:
                <input id="userPhone" name="User[phone]" type="text" value="{{app\helpers\User::get()->value('phone')}}">
            </label><br/>
            <label for="userEmail">
                Email:
                <input id="userEmail" name="User[email]" type="text" value="{{app\helpers\User::get()->value('email')}}">
            </label><br/>
            <input type="submit" class="button checkout" value="Submit"/>
        </form>
    </div>
</div>
{% endblock %}