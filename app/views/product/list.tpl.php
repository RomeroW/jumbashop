{% extends:'layout' %}
{% block:content %}
<div id="products" class="row">
    <ul class="large-block-grid-4 medium-block-grid-3 small-block-grid-1">
        {% foreach($products as $product) %}
        <li>
            <div class="product-item">
                <div class="image">
                    <img src="{{$product->getImageLink()}}">
                </div>
                <div class="info">
                    <a class="title" href="/product/{{$product->id}}">{{$product->title}}</a>
                    <div class="bottom rows">
                        <a class="button item-to-cart" href="{{$product->getBuyLink()}}">To cart</a>
                        <div class="price">{{$product->price}}$</div>
                        <div style="clear: both"></div>
                    </div>
                </div>
            </div>
        </li>
        {% endforeach %}
    </ul>
</div>
{% endblock %}