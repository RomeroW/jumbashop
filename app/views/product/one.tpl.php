{% extends:'layout' %}
{% block:content %}

<div id="product" class="row">
    <div class="columns small-5">
        <div id="product-image">
            <div class="main-image">
                <img id="main-image" src="">
            </div>

            {% if(count($images)>0) %}
            <ul class="mini-images">
                {% foreach($images as $i) %}
                <li>
                    <img src="{{$i->link}}" alt="{{$i->title}}"/>
                </li>
                {% endforeach %}
            </ul>
            {% endif %}
        </div>
        <a class="button to-cart" href="{{$product->getBuyLink()}}">
            <div class="small-cart-icon"></div>
            <div class="button-label">Add to cart</div>
        </a>
    </div>
    <div class="columns small-7">
        <div class="one-product">
            <h1 class="title">
                {{$product->title}}
            </h1>
            <div class="price">
                {{$product->price}}$
            </div>
            <div class="description">{{$product->description}}</div>
        </div>
    </div>
</div>
<div class="row line"></div>

{% if(count($relatedProducts) > 0) %}
<div id="products" class="row">
    <h2 class="also">People also viewed</h2>
    <ul class="large-block-grid-4 medium-block-grid-3 small-block-grid-1">
        {% foreach($relatedProducts as $p) %}
        <li>
            <div class="product-item">
                <div class="image">
                    <img src="{{$p->link}}">
                </div>
                <div class="info">
                    <a class="title" href="/product/{{$p->id}}">{{$p->title}}</a>
                    <div class="bottom rows">
                        <a class="button item-to-cart" href="{{app\models\Product::getBuyLinkS($p->id)}}">To cart</a>
                        <div class="price">{{$p->price}}$</div>
                        <div style="clear: both"></div>
                    </div>
                </div>
            </div>
        </li>
        {% endforeach %}
    </ul>
</div>
<div class="row line"></div>
{% endif %}

{% if(count($boughtProducts) > 0) %}
<div id="products" class="row">
    <h2 class="also">People also bought</h2>
    <ul class="large-block-grid-4 medium-block-grid-3 small-block-grid-1">
        {% foreach($boughtProducts as $p) %}
        <li>
            <div class="product-item">
                <div class="image">
                    <img src="{{$p->link}}">
                </div>
                <div class="info">
                    <a class="title" href="/product/{{$p->id}}">{{$p->title}}</a>
                    <div class="bottom rows">
                        <a class="button item-to-cart" href="{{app\models\Product::getBuyLinkS($p->id)}}">To cart</a>
                        <div class="price">{{$p->price}}$</div>
                        <div style="clear: both"></div>
                    </div>
                </div>
            </div>
        </li>
        {% endforeach %}
    </ul>
</div>
<div class="row line"></div>
{% endif %}

{% if(count($inCartProducts) > 0) %}
<div id="products" class="row">
    <h2 class="also">People also put to the cart</h2>
    <ul class="large-block-grid-4 medium-block-grid-3 small-block-grid-1">
        {% foreach($inCartProducts as $p) %}
        <li>
            <div class="product-item">
                <div class="image">
                    <img src="{{$p->link}}">
                </div>
                <div class="info">
                    <a class="title" href="/product/{{$p->id}}">{{$p->title}}</a>
                    <div class="bottom rows">
                        <a class="button item-to-cart" href="{{app\models\Product::getBuyLinkS($p->id)}}">To cart</a>
                        <div class="price">{{$p->price}}$</div>
                        <div style="clear: both"></div>
                    </div>
                </div>
            </div>
        </li>
        {% endforeach %}
    </ul>
</div>
<div class="row line"></div>
{% endif %}

{% endblock %}