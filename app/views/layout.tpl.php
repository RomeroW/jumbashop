<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="{{ $__charset__ }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Jumba Shop</title>
    <link rel="stylesheet" href="/assets/css/foundation.css"/>
    <link rel="stylesheet" href="/assets/css/projects.css"/>
    <script src="/assets/js/vendor/modernizr.js"></script>
</head>

<body>
<header class="small-12 row">
    <div id="logo" class="small-6 columns">Jumba shop</div>
    <div id="right" class="small-6 columns ">
        <div class="actions">
            <a class="cart" href="/cart">
                <div class="icon-cart"></div>
                <div class="header">Cart</div>
                <div class="products">{{count(app\helpers\User::get()->cart->getProducts())}}</div>
            </a>
            <!--<div class="checkout-container">
                Checkout
            </div>-->
        </div>
    </div>
</header>
{{block:content}}{{endblock}}
<script src="/assets/js/vendor/jquery.js"></script>
<script src="/assets/js/foundation.min.js"></script>
<script>
    $(document).foundation();

    function initImagesViewer(){
        var srcPath = $('.mini-images li:first-child').find('img').attr('src');

        $('.main-image').find('img').attr('src', srcPath);

        $('.mini-images li, .mini-images li img').click(function (event){
            $('.mini-images').children().removeClass('active');
            $(event.target).addClass('active');
            $('.main-image').find('img').attr('src', $(event.target).find('img').attr('src'));
        });

        $('.mini-images li img').click(function (event){
            $('.mini-images').children().removeClass('active');
            $('.main-image').find('img').attr('src', $(event.target).attr('src'));
        });
    }

    window.onload(initImagesViewer());
</script>
</body>
</html>
