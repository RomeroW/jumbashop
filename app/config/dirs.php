<?php

/**
 * default directories
 */

return array(
    //target configs folder (all configs store here)
    'target' => 'config/',

    //dev.json, prod.json etc located here
    'environments' => 'config/env/',

    //master.json here
    'master' => 'config/env/',
);