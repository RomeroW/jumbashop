<?php
if(!file_exists(dirname(__FILE__) . '/errorhandle.php')) {
    return
        [
            'log_errors'     => true,
            'display_errors' => 0
    ];
}
else {
    include_once('errorhandle.php');
}