<?php

return 
[
	'default' => 'localhost',
	
	'configurations' => 
	[
		'localhost' =>
		[
			'dsn'         => 'mysql:dbname={{db.name}};host={{db.host}};port={{db.port}}',
			'username'    => '{{db.user}}',
			'password'    => '{{db.pass}}',
			'persistent'  => false,
			'log_queries' => false,
			'reconnect'   => false,
			'queries'     => 
			[
				"SET NAMES UTF8",
			],
		],
	],
];