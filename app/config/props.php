<?php
/**
 * Prop files
 */
return array(
    array(
        'source' => 'config/props/database.prop.php',
        'target' => 'config/database.php'
    ),
    array(
        'source' => 'config/props/nginx.prop.conf',
        'target' => 'config/nginx.conf'
    ),
    array(
        'source' => 'config/props/errorhandle.prop.php',
        'target' => 'config/errorhandle.php'
    )
);